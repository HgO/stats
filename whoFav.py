#!/usr/bin/python3
from mastodon import Mastodon

from getpass import getpass
import os
import time

import matplotlib
import numpy as np
import matplotlib.pyplot as plt

from collections import defaultdict

import json

def plot_data(fav_dict, title, output="graph.png"):
    nbFav = sum(fav_dict.values())
    num_bins = len(fav_dict)
    
    n, bins, patches = plt.hist(sorted(fav_dict.values(), reverse=True), num_bins)

    plt.title(title.format(favs=nbFav, users=num_bins))
    plt.xlabel("Nombre de favs")
    plt.ylabel("Personnes")
    
    if output:
        plt.savefig(os.path.join(data_dir, output))

    plt.show()
    
def show_top(fav_dict, output="top.txt", n=50):
    print("Bilan : ")
    
    limit = min(n, len(fav_dict))
    with open(os.path.join(data_dir, output.format(n=n)), "w+") as f:
        for i, name in enumerate(sorted(fav_dict, key=fav_dict.get, reverse=True), 1):
            if i > limit:
                break
            
            info = "rang " + str(i) + " : " + name + " avec " + str(fav_dict[name]) + " favs"
            print(info)
            f.write(info + "\n")


instance = input("Your instance (eg: mastodon.social):")
api_base_url = "https://" + instance
username = "%s@%s" % (input("Your username (without the instance part):"), instance)

data_dir = os.path.join("data", username)
users_dir = os.path.join(data_dir, "users")

clientcredfilename  = "pytooter_clientcred.secret"
usercredfilename    = os.path.join(data_dir, "pytooter_usercred.secret")

try:
    os.makedirs(users_dir)
except OSError:
    pass

# Register app - only once!
if(not os.path.exists(clientcredfilename)):
    Mastodon.create_app(
         'whoFavApp',
         api_base_url = api_base_url,
         to_file = clientcredfilename
    )

# Log in to your account, once
if(not os.path.exists(usercredfilename)):
    mastodon = Mastodon(
        client_id = clientcredfilename,
        api_base_url = api_base_url
    )
    mastodon.log_in(
        input("E-mail used to create your account (%s): " % username),
        getpass("Password for %s: " % username),
        to_file = usercredfilename
    )


# Create actual API instance
mastodon = Mastodon(
    client_id = clientcredfilename,
    access_token = usercredfilename,
    api_base_url = api_base_url
)

# Restore the last toot ID
max = None
i = 0
if(os.path.exists(os.path.join(data_dir, "lastID"))):
    with open(os.path.join(data_dir, "lastID")) as f:
        max, i = map(int, f.read().split())
        
        

user = mastodon.account_verify_credentials()

# 1) We fetch all your favs 
favs = mastodon.favourites(limit=40)

if os.path.exists(os.path.join(data_dir, "favourites.json")):
    with open(os.path.join(data_dir, "favourites.json")) as f:
        fav_dict = defaultdict(lambda: 0, json.load(f))
else:
    fav_dict = defaultdict(lambda: 0)
    while favs != None and len(favs) > 0:
        t1 = time.time()
        print("\x1b[2K\rfetching your favs", end = "\033...")
        for fav in favs:
            print("\x1b[2K\rTreating fav %d" % fav["id"], end = "...")

            u = fav['account']
            print("\x1b[2K\rstatus %d was posted by %s" % (fav["id"], u["username"]), end = ".")
            fav_dict[u["acct"]] += 1
            
        filename = os.path.join(data_dir, "favourites.json")
        with open(filename, "w+") as f:
            json.dump(fav_dict, f, ensure_ascii=False, indent=4, sort_keys=True)
        
        favs = mastodon.fetch_next(favs)
        t2 = time.time()
        if(t2 -t1 <= 1):
            time.sleep(1-(t2-t1))

# 1.1) We show all your crushs

show_top(fav_dict, output="top{n}-favourites.txt")

plot_data(fav_dict, 
    title="Répartition de mes {favs} favs \nentre les {users} personnes que j'ai fav.",
    output="graph-favourites.png"
)


# 2) We fetch all your toots

print("fetching your toots", end = "\033...")
toots = mastodon.account_statuses(user['id'], max_id = max, limit = 40)
nbToots = user['statuses_count']

fav_by_dict = defaultdict(lambda: 0, {})
if os.path.exists(os.path.join(data_dir, "favourited-by.json")):
    with open(os.path.join(data_dir, "favourited-by.json")) as f:
        fav_by_dict = defaultdict(lambda: 0, json.load(f))

while(toots != None and len(toots) > 0):
    t1 = time.time()
    print("\x1b[2K\rfetching your toots", end = "\033...")
    for toot in toots:
        i += 1
        print("\x1b[2K\rTreating toot %d (%d/%d)" % (toot["id"], i, nbToots), end = "...")

        if(toot["favourites_count"] > 0 and toot["account"]["id"] == user["id"]):
            max = toot["id"]
            with open(os.path.join(data_dir, "lastID"), "w+") as f:
                f.write("%d %d" % (max, i-1))
            
            t2 = time.time()
            if(t2 -t1 <= 1):
                time.sleep(1-(t2-t1))
            users = mastodon.status_favourited_by(toot["id"])
            t1 = time.time()
            
            for u in users:
                print("\x1b[2K\rstatus %d was favourited by %s" % (toot["id"], u["username"]), end = ".")
                
                fav_by_dict[u["acct"]] += 1
                
            with open(os.path.join(data_dir, "favourited-by.json"), "w+") as f:
                json.dump(fav_by_dict, f, ensure_ascii=False, indent=4, sort_keys=True)
    
    toots = mastodon.fetch_next(toots)
    t2 = time.time()
    if(t2 -t1 <= 1):
        time.sleep(1-(t2-t1))

# 2.1) We show who is crushing on you

show_top(fav_by_dict, output="top{n}-favourited-by.txt")

plot_data(fav_by_dict, 
    title="Répartition des {favs} favs \nentre les {users} personnes qui ont déjà fav un de mes statuts.",
    output="graph-favourited-by.png"
)
